import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';
import useAsync from './hooks/useAsync';

const apiAddress = 'http://localhost:80/login';

function App() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [validatePassword, setValidatePassword] = useState('');

  const handleChange = handler => e => handler(e.target.value);

  useEffect(() => {
    axios.post(`${apiAddress}/getUsers`).then(({ data }) => console.log('registerd users', data))
  }, [])
  useAsync()

  return (
    <div className="App">
      <div>
        <label>Username</label>
        <input type="text" value={username} onChange={handleChange(setUsername)} />
      </div>
      <div>
        <label>Password</label>
        <input type="password" value={password} onChange={handleChange(setPassword)} />
      </div>
      <div>
        <label>Validate Password</label>
        <input type="password" value={validatePassword} onChange={handleChange(setValidatePassword)} />
      </div>
      <div>
        <input type="button" value="submit" onClick={() =>
          password === validatePassword && axios
            .post(`${apiAddress}/register`, { username, password }, { headers })} />
      </div>
    </div>
  );
}

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
}

export default App;
