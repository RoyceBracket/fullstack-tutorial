import { useState } from 'react';

export default (getter, initial) => {
    const [data, setData] = useState(initial);
    const [error, setError] = useState();

    const invoke = (...args) => {
        getter(...args)
            .then(({ status, data }) => parseInt(status) === 200
                ? setData(data)
                : setError(data))
            .catch(setError);
    }

    return { data, error, invoke, setError };
}