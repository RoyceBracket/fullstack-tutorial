const express = require('express');
const session = require('express-session');
const routers = require('./routers');

const app = express();
const port = 80;

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    next();
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(session({
    secret: 'bestSecretEver',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}))

Object.entries(routers).forEach(([route, router]) => app.use(`/${route}`, router));
app.listen(port, () => console.log(`server is listening on port ${port}`));