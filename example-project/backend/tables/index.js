const DB = require('../utils/db');

const db = new DB(__dirname);

//create tables
[
    'users'
].forEach(table => db.createTable(table));

module.exports = db;