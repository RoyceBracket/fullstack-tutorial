module.exports = db => class {
    login(req, res) {
        const { password, username } = req.body;
        const userId = db.getTable('users')
            .getAll()
            .findIndex(user => user.username === username &&
                user.password === password)

        if (userId > 0) res.status(404).send();
        else {
            req.session.userId = userId;
            res.status(200).send();
        };
    }

    profile(req, res) {
        return res.json(req.session.userId || false)
    }

    register(req, res) {
        const { password, username } = req.body;
        if (!password || !username) return res.send(400);

        try { db.getTable('users').add({ password, username }).save(); }
        catch (e) {
            console.error(e);
            res.status(500).send();
        }

        res.status(200).send();
    }

    getUsers(req, res) {
        res.status(200).json(db.getTable('users').getAll())
    }

    logout() {
        req.session.destroy();
        res.status(200).send();
    }
}