const fs = require('fs');
const path = require('path');

const defaultKeyMap = key => key
    .split('-').join('::')
    .split('_').join('::')
    .split(' ').join('::')
    .split('::').map((word, idx) => !idx ? word : word.charAt(0).toUpperCase() + word.slice(1))

module.exports = (dir, {
    mapKey = defaultKeyMap,
    exclude = ['index.js']
} = {}) => fs.readdirSync(dir)
    .filter(file => !exclude.includes(file))
    .reduce((acc, file) => ({
        ...acc, [mapKey(file)]: require(path.join(dir, file))
    }), {})