const fs = require('fs');
const path = require('path');

module.exports = class {
    constructor(dir) {
        this.dir = dir;
        this.tables = {};
    }

    getTable(name = '') { return this.tables[name] }
    hasTable(name = '') { return !!this.table[name]; }

    createTable(name = '') {
        if (this.tables[name]) throw Error('table exist');
        else this.tables[name] = new Table(path.join(this.dir, `${name}.json`));

        return this;
    }
}

class Table {
    constructor(tablePath) {
        this.tablePath = tablePath;
        if (!fs.existsSync(tablePath)) fs.writeFileSync(tablePath, '[]');

        this.data = JSON.parse(fs.readFileSync(tablePath));
    }

    save() { fs.writeFileSync(this.tablePath, JSON.stringify(this.data, null, 4)); }
    getAll() { return this.data }

    add(...items) {
        this.data.push(items)
        return this;
    }
}