const { Router } = require('express');
const tables = require('./tables');
const controllers = require('./controllers');

module.exports = Object.entries(controllers)
    .map(([name, controller]) => [name, controller(tables)])
    .map(([name, Controller]) => [name, new Controller()])
    .reduce((acc, [name, controller]) => ({
        ...acc, [name]: Object.getOwnPropertyNames(Object.getPrototypeOf(controller))
            .reduce((router, method) => !typeof method === 'function' ? router :
                router.post(`/${method}`, controller[method].bind(controller)), new Router())
    }), {});