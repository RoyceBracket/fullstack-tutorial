# Fullstack Tutorial
*this repo currently being updated hourly*
## What do I need?

- internet connection
- computer (pc\laptop). preferably windows because i'm not familiar with other platforms and wont be able to assist

## Background requirements?

Im going to show the process of basic app development. it will show the quickest why to implement  
a frontend (web-application) & backend (server),
demonstrating the daily array of tasks requested from a fullstack position and the life-cycle of program.  
  
If you know code fundementals but lack the concepts of how to assamble it to an app you are in the right place.  
If you want to know how the development process of an app looks and what are the involved ingeridiants,
a shortend version of this content will be available in the future.  
If you are looking to learn how to code\coding fundementals, you are NOT the right place.

## What are we going learn?

1) Git gud with GIT

- What is git
- Git & terminal basic usage
- Setup & view the example project
- Setup reposetory for your project (setting started)

2) How to build a front end (web application) with a login page. including:

- HTTP protocol - a bit about how our browser communicates with the world
- html, css - how our browser render our communication with the world
- JavaScript - the main tool with which we'll manipulate our app to do our beedings (';..;')

3) How to build a back end (server), that will manage our businnes logic. including

- NodeJS - a JS framework to develop backend tools
- ExpressJS - a nodejs library for developing servers

you can [start](/git/1.md) with the first chapter, Git Gud With Git
